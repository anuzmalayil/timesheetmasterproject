import { Component, OnInit, OnDestroy ,Input } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { WeekDayService } from '../services/weekday.service';
import { TaskEffortService } from '../services/taskeffort.service';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
@Component({
    selector: 'log-effort',
    templateUrl: 'logeffort.component.html',
    styleUrls: ['./logeffort.component.scss']
})

export class LogEffortComponent implements OnInit, OnDestroy {
    @Input() selectedEmployeeId: number;
    employees: any;
    private sub: any;
    taskEfforts: any;
    tasks: any;
    weekdays: any;
    selectedEmployee: any;
    currentWeekId: number = 1;
    addForm: FormGroup;
    logeffortModel: any = {};
    
    constructor(private employeeService: EmployeeService,
        private weekDayService: WeekDayService,
        private taskService: TaskService,
        private taskEffortService: TaskEffortService,
        private router: Router, private route: ActivatedRoute
    ) { }

    ngOnInit() {


        this.route
            .queryParams
            .subscribe(params => {

                if (params['weekId']) {
                    this.currentWeekId = params['weekId'];
                    
                }


                this.taskEffortService.getEmployeesEffortByWeek(this.currentWeekId).subscribe(data => {
                    this.employees = data;
                });
                this.getWeekDayList();
                this.getTasksList();

            });

     





    }

    public onTaskChange(event): void {  // event will give you full breif of action
        this.logeffortModel.taskId  = event.target.value;
  
      }
      public onWeekDayChange(event): void {  // event will give you full breif of action
        this.logeffortModel.weekDayId  = event.target.value;
  
      }
    onSubmit() {

        this.logeffortModel.WeekId=this.currentWeekId;
        this.logeffortModel.EmployeeId=this.selectedEmployeeId;
        
        this.taskEffortService.createTaskEffort(JSON.stringify(this.logeffortModel) );
       
      
     
    }

    getTasksList() {

        this.taskService.getAllTasks().subscribe(data => {
            this.tasks = data;
        });

    }
    getWeekDayList() {
        this.weekDayService.getAllWeekDays().subscribe(data => {
            this.weekdays = data;
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    public viewTimeSheet(id) {

        this.router.navigate(['/timesheet'], { queryParams: { employeeId: id } })



    }
}