 

    import { ModuleWithProviders }  from '@angular/core';  
import { Routes, RouterModule } from '@angular/router';  
  
import { TimesheetListComponent } from './timesheet/timesheet.component';
import { EmployeeListComponent } from './employee/employee.component';
// Route Configuration  
export const routes: Routes = [  
  {  
    path: '',  
    component: EmployeeListComponent,
    pathMatch: 'full'  
  },  
  {  
    path: 'employeelist',  
    component: EmployeeListComponent,
    pathMatch: 'full'  
  }, 
  {  
    path: 'timesheet',  
    component: TimesheetListComponent, 
    pathMatch: 'full'  
  },  
 
];  
  
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);  

