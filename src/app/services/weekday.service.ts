import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WeekDayService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getAllWeekDays() {
        return this.http.get(this.baseapi + "/weekday/getall");
    }
}