import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TimeSheetService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

 

    getEmployeeTimesheetByWeek(employeeId,weekId) {
        return this.http.get(this.baseapi + "/timesheet/+"+employeeId +"/"+weekId );
    }


}