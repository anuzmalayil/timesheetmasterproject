import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient ,HttpHeaders } from '@angular/common/http';

@Injectable()
export class TaskEffortService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getEmployeesEffortByWeek(weekId) {
        return this.http.get(this.baseapi + "/timesheet/"+weekId);
    }

    createTaskEffort(data) {
       
        let headers = new HttpHeaders({ 'Content-Type': 'application/JSON' });
        
        this.http.post<any>(this.baseapi+ "/timesheet/", data, { headers: headers })
          .subscribe(
                result => { console.log(result); },
                error => {  },
                () => {});
        }


      //  return this.http.post(this.baseapi+ "/timesheet/" , logeffortModel);
    }
 