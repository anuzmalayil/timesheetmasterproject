import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getAllemployees() {
        return this.http.get(this.baseapi + "/employee/getall");
    }


    getEmployeesEffortByWeek() {
        return this.http.get(this.baseapi + "/employee/1");
    }


}