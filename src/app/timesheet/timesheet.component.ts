

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TimeSheetService } from '../services/timesheetservice';
import { EmployeeService } from '../services/employee.service';
import { Router } from "@angular/router";
@Component({
  selector: 'timesheet-list',
  templateUrl: 'timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})


export class TimesheetListComponent implements OnInit {
  timesheet: any;
  id: number;
  logEffortComponentVisibility: boolean;

  private sub: any;
  employeesList: any;
  currentweekNo: number;
  employeeId: number;
  opt: any;
  constructor(private route: ActivatedRoute, private router: Router, private timeSheetService: TimeSheetService, private employeeService: EmployeeService) { }



  ngOnInit() {
    this.logEffortComponentVisibility = false;
    this.route
      .queryParams
      .subscribe(params => {

        this.employeeId = params['employeeId'];
        this.currentweekNo = 1;
        this.loadtimesheetsByWeek(this.employeeId);
        this.getEmployees();


      });


  }

  public onEmployeeChange(event): void {  // event will give you full breif of action
    const employeeId = event.target.value;
    console.log(employeeId);
    this.loadtimesheetsByWeek(employeeId);



  }

  loadtimesheetsByWeek(employeeId) {
    this.timeSheetService.getEmployeeTimesheetByWeek(this.currentweekNo, employeeId).subscribe(data => {
      this.timesheet = data;

    });
  }
  public prevWeekClick(event): void {  // event will give you full breif of action

    if (this.currentweekNo > 1)
      this.currentweekNo = this.currentweekNo - 1;
    this.loadtimesheetsByWeek(this.employeeId);
  }
  public nextWeekClick(event): void {  // event will give you full breif of action

    this.currentweekNo += 1;
    this.loadtimesheetsByWeek(this.employeeId);
  }

  public logEffortClick(event): void {  // event will give you full breif of action

 this.logEffortComponentVisibility=true;
  }

  public backtoList(event): void {  // event will give you full breif of action

    this.logEffortComponentVisibility=false;


    this.router.navigate(['/employeelist'], { queryParams: { weekId: this.currentweekNo } })
  }

  public getEmployees() {


    this.employeeService.getAllemployees().subscribe(data => {
      this.employeesList = data;
    }

    )
  }





}