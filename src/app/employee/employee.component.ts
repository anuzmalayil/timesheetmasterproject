import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TaskEffortService } from '../services/taskeffort.service';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls: ['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    private sub: any;
    taskEfforts: any;
    selectedEmployee: any;
    currentWeekId: number = 1;
    constructor(private employeeService: EmployeeService, private taskEffortService: TaskEffortService, private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {


        this.route
            .queryParams
            .subscribe(params => {

                if (params['weekId']) {
                    this.currentWeekId = params['weekId'];
                }


                this.taskEffortService.getEmployeesEffortByWeek(this.currentWeekId).subscribe(data => {
                    this.employees = data;
                });


            });

        // this.employeeService.getAllemployees().subscribe(data => {
        //     this.employees = data;
        // });





    }


    public viewTimeSheet(id) {

        this.router.navigate(['/timesheet'], { queryParams: { employeeId: id } })


    }
}