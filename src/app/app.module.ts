import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TaskService } from './services/task.service';
import { WeekDayService } from './services/weekday.service';
import { TaskEffortService } from './services/taskeffort.service';
import { TimeSheetService } from './services/timesheetservice';
import { TimesheetListComponent } from './timesheet/timesheet.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogEffortComponent } from './logeffort/logeffort.component';
 
import { routing } from './app.routes';  
// const routes: Routes = [
 
//   {
//     path: 'items',
//     component: TimesheetListComponent
//   }
// ];

const  routes=RouterModule.forRoot(
  [
    { path: "", component: TimesheetListComponent}
  ]
)
@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetListComponent,
    LogEffortComponent

  ],
  // imports: [
  //   BrowserModule,
  
  //   HttpClientModule,   RouterModule.forRoot(routes),
  // ],
  imports: [routing  ,BrowserModule,HttpClientModule,ReactiveFormsModule,FormsModule],
  exports: [RouterModule,   FormsModule,
    ReactiveFormsModule],
  
  providers: [
    EmployeeService,
    TaskService,
    TaskEffortService,
    TimeSheetService,
    WeekDayService
  
  ],

 
 
  bootstrap: [AppComponent]
})
export class AppModule { }



 